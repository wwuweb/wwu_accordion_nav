
// // // // // // //      Accordion Navigation      // // // // // // //
$(".site-nav li").has("ul").addClass("is-accordion is-closed");
$(".site-nav .is-closed > ul").hide();

$(document).ready(function() {
	$(".site-nav .is-open > a").attr("aria-expanded", "true");
	$(".site-nav .is-closed > a").attr("aria-expanded", "false");
});

// if child of ul and it's a parent of another ul
$(".site-nav .is-accordion").click(function(event) {
	var isAccordion = $(this);

	event.stopPropagation();
	isAccordion.toggleClass("is-closed is-open");
	if (isAccordion.hasClass("is-open")) {
		isAccordion.children("a").attr("aria-expanded", "true");
	}
	else {
		isAccordion.children("a").attr("aria-expanded", "false");
	}
	isAccordion.children("ul").slideToggle();
});


// Prevents $("a").click() from bubbling up to parent <li> if it doesn't have a nested/child accordion
var stopNestedBubbling = $(".site-nav li").not(":has(ul)");

stopNestedBubbling.click(function(event) {
	event.stopPropagation();
});

// //      Active Accordion Anchors and Parents     // //
// Styles for active nav items and nav item parents

var currentLocation = window.location.href.split('/');
var path = window.location.href.split('/');
var page = path[path.length - 1];

$('a[href*="' + page + '"]').each(function() {
  if (window.location.href == this.href) {
    $(this).addClass("is-active");
  }
});

$("a[href='#']").click(function(e) {
	e.preventDefault();
});

$(".site-nav .is-active").parents(".is-accordion").addClass("is-active-parent");
$(".site-nav .is-active-parent ul").show();
$(".site-nav .is-active-parent").toggleClass("is-closed is-open");


//Event handler to call the new accordion menu behavior
$(".main-nav li.is-accordion").on("click", accordionOption);

/**
* Function to define a new behavior for the standard accordion menu.
* This behavior only allows one accordion menu to be open at any given
* time. If an additional accordion is opened, it will close all others.
* Behavior accounts for single accordion menus, as well as singley-nested
* accordion menus.
*/

//Before accordionOption does it's thing, we need to check whether or not their is an activeNested Child

function accordionOption(){
  var liActiveNested = $("li.activeNested");
	var liActive = $("li.active");
	var accordionItem = $(this);

	if (accordionItem.hasClass("nestedMenu")){ //checks if current menu clicked is a nested menu
		if(accordionItem.hasClass("activeNested")){ //checks if the nested menu is already open.  If so, close.
			accordionItem.removeClass("activeNested").find("ul").slideUp();
			accordionItem.css('background-image', 'url(' + "https://www.wwu.edu/wwucommon/lite/images/accordion-plus-icon.png" + ')');
		}else{ //open nested menu, and close all other nested menus -- does not close parent menu.
			liActiveNested.css('background-image', 'url(' + "https://www.wwu.edu/wwucommon/lite/images/accordion-plus-icon.png" + ')');
			liActiveNested.removeClass("activeNested is-open").addClass("is-closed").find("ul").slideUp();
			$("li.nestedMenu.is-closed").children("a").attr("aria-expanded", "false");
			accordionItem.find("ul").slideDown();
			accordionItem.css('background-image', 'url(' + "https://www.wwu.edu/wwucommon/lite/images/accordion-minus-icon.png" + ')');
			accordionItem.addClass("activeNested");
		}
	}else{ //Not a nested menu
		if(accordionItem.hasClass("active")){ //check if the menu is currently open. If so, close.
			accordionItem.children("ul").children(".activeNested").removeClass("activeNested").css('background-image', 'url(' + "https://www.wwu.edu/wwucommon/lite/images/accordion-plus-icon.png" + ')').find("ul").slideUp();
			accordionItem.removeClass("active").find("ul").slideUp();
			accordionItem.css('background-image', 'url(' + "https://www.wwu.edu/wwucommon/lite/images/accordion-plus-icon.png" + ')');
		}else{ //open menu, and close all others, including nested menus.
			liActive.css('background-image', 'url(' + "https://www.wwu.edu/wwucommon/lite/images/accordion-plus-icon.png" + ')');
			liActive.children("ul").children(".activeNested").removeClass("activeNested is-open").addClass("is-closed").css('background-image', 'url(' + "https://www.wwu.edu/wwucommon/lite/images/accordion-plus-icon.png" + ')').find("ul").slideUp();
			liActive.removeClass("active is-open").addClass("is-closed").find("ul").slideUp();
			$("li.is-closed").children("a").attr("aria-expanded", "false");
			accordionItem.addClass("active").css('background-image', 'url(' + "https://www.wwu.edu/wwucommon/lite/images/accordion-minus-icon.png" + ')');
			accordionItem.children("ul").children("li.is-accordion").addClass("nestedMenu");

		}
	}
}


 /**
 * Function to close all sub-accordion menus when a link is clicked when they are open.
 * Fixes a bug where sub-menus would stay open when user had opened a submenu, but clicked
 * a link outside of that submenu but still within the parent menu.  This function
 * ensures that all menus that do not contain the current active link are closed.
 */

function closeNonActive(){

 	if($("li.is-active-parent").first().find("ul").find("li.is-accordion").length){ //Check whether there are sub-accordions in the first place

 		//Loop through all submenus and tag the one (if any) that has the active link within.
 		$("li.is-active-parent").first().find("ul").find("li.is-accordion").each(function(){

 			//check if the links in submenu have current active link in them. If so, switch icon, and tag parent ul with "stayOpen" class for later uses.
 			$(this).find("ul").find("a").each(function(){
 				if($(this).attr("href").indexOf(page) > -1){ //We found a submenu page!

					$(this).parent().parent().removeClass("is-closed").addClass("is-open stayOpen"); //add the class "stayOpen"
					$(this).parent().parent().css("background-image", 'url(' + "https://www.wwu.edu/wwucommon/lite/images/accordion-minus-icon.png" + ')'); //correct image.
 					}
 			});

 			//loop through all submenus and close them if they do not contain the active link.
 			$(this).find("ul").find("a").each(function(){
 				if(!$(this).parent().parent().hasClass("stayOpen")){
 					$(this).parent().parent().slideUp();
 					$(this).parent().parent().addClass("is-closed").removeClass("is-open");
 				}
 			})

 		})
 	} //end wrapper if statement
 }

/**
* This function sets up the correct classes on a page onLoad, for example adding "active" class to open
* accordions, and "activeNested" on open sub-accordions.  This allows the behavior defined in the closeNonActive
* to be accurate on a fresh page reload.
*/

 function menuPrimer(){
 	$("li.is-active-parent").first().addClass("active is-open").removeClass("is-closed");
 	if($("li.is-active-parent").first().find("ul").find("li.is-accordion").length){
 		$("li.is-active-parent").first().find("ul").find("li.is-accordion").addClass("nestedMenu");
 		$("li.is-active-parent").first().find("ul").find("li.is-active-parent").addClass("activeNested");
 	}
 }

/**
* This function primes all include files that rely on javascript, such as the menu and the tagline features.
*/

function includesConfig(){
	menuPrimer();
	closeNonActive();
	checkTagLine();
}

/**
* This function checks if there is a tagline for the site. If not, nothing happens.  If there is a tagline, this script
* moves the title of the site up to account for the tagline.
*/

function checkTagLine(){
    if($("#tagLine").length) //check if the tagLine element exists.  This protects against strange behavior for websites that do not have the newest version of the lite-template.
		if(!($("#tagLine").text().trim() === ""))
			$(".hide-desktop").css("bottom", "20px");
}





 //calls menuConfig when include file is loaded...
 window.onload = includesConfig;
